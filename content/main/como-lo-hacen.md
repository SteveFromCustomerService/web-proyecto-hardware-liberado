+++
title = '¿Cómo funciona?'
draft = false
+++




<div class="row" style="display:flex; flex-wrap:wrap; padding: 0 4px;">
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <h3>Recogida de Equipos</h3>
    <p>Se recogen <b>ordenadores desechados</b> de la Universidad de Zaragoza y equipos sustitutos.</p>
  </div>
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <img src="/img/presentacion_proyection/ciclo_proyecto_1.png" alt="ciclos" style="height:auto; max-height:75%;"/>
  </div>
</div>


<div class="row" style="display:flex; flex-wrap:wrap; padding: 0 4px;">
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <h3>Revisión, Clasificación y Documentación</h3>
    <p>Los equipos se someten a una revisión exhaustiva, <b>se clasifican</b> en función de su estado y <b>se documentan</b> para determinar su destino.</p>
  </div>
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <img src="/img/presentacion_proyection/ciclo_proyecto_2.png" alt="ciclos" style="height:auto; max-height:75%;"/>
  </div>
</div>


<div class="row" style="display:flex; flex-wrap:wrap; padding: 0 4px;">
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <h3>Reacondicionamiento</h3>
    <p><b>Reacondicionamos</b> equipos que llegan al final de su ciclo de uso. Estos equipos se documentan y se instalan con una versión de Linux y una configuración de escritorio, lo que les permite tener una segunda vida útil.</p>
  </div>
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <img src="/img/presentacion_proyection/ciclo_proyecto_3.png" alt="ciclos" style="height:auto; max-height:75%;"/>
  </div>
</div>


<div class="row" style="display:flex; flex-wrap:wrap; padding: 0 4px;">
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <h3>Reciclado de Equipos</h3>
    <p>Los equipos que ya no pueden ser reacondicionados o utilizados se envían a un <b>punto limpio</b> después de realizar un borrado seguro. Esto garantiza que los componentes sean manejados de manera responsable y no causen impactos ambientales negativos.</p>
  </div>
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <img src="/img/presentacion_proyection/ciclo_proyecto_4.png" alt="ciclos" style="height:auto; max-height:75%;"/>
  </div>
</div>


<div class="row" style="display:flex; flex-wrap:wrap; padding: 0 4px;">
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <h3>Exposición para Venta o Cesión</h3>
    <p>Los equipos reacondicionados se dan de alta en nuestra web app para su exposición. Llevamos a cabo una gestión del inventario meticulosa para mantener un control efectivo de las ventas y las cesiones.</p>
  </div>
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <img src="/img/presentacion_proyection/ciclo_proyecto_5.png" alt="ciclos" style="height:auto; max-height:75%;"/>
  </div>
</div>


<div class="row" style="display:flex; flex-wrap:wrap; padding: 0 4px;">
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <h3>Seguimiento "Posventa"</h3>
    <p>Para alcanzar nuestro doble objetivo de favorecer la economía circular y fomentar el uso del software libre, <b>realizamos un seguimiento automático</b> del uso de los equipos y mantenemos un programa de formación para los usuarios. Esto asegura un uso eficiente de los recursos y promueve prácticas sostenibles.</p>
  </div>
  <div class="column" style="flex: 50%; padding: 0 4px;">
    <img src="/img/presentacion_proyection/ciclo_proyecto_6.png" alt="ciclos" style="height:auto; max-height:75%;"/>
  </div>
</div>

