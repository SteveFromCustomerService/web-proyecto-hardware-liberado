+++
title = 'Acerca del proyecto'
draft = false
id = 'acerca-del-proyecto'
+++


Desde el SICUZ y la OSLUZ hemos iniciado un proyecto sostenible para transformar la manera en que manejamos nuestros equipos informáticos. Nuestra misión es clara: **reducir el impacto ambiental de la tecnología**, fomentar el **uso de software libre**, y trabajar hacia los **Objetivos de Desarrollo Sostenible (ODS)**. 



### Cuidando el Ciclo de Vida de los Ordenadores

Nuestro proyecto busca reinventar el ciclo de vida de los equipos internos de la universidad para maximizar la eficiencia y la sostenibilidad.



### Nuestros Objetivos


 - Dar una segunda oportunidad al material informático "desechado" en la Universidad de Zaragoza, extendiendo su vida útil y reduciendo el desperdicio electrónico.

 - Formar técnicos en la revisión, reparación y reacondicionado de equipos informáticos, brindando oportunidades de aprendizaje y desarrollo de habilidades.

 - Fomentar el uso de Software Libre, promoviendo la libertad y la colaboración en el mundo del software.

 - Ayudar al cumplimiento de los Objetivos de Desarrollo Sostenible (ODS), específicamente los ODS 8 (Trabajo Decente y Crecimiento Económico), 12 (Producción y Consumo Responsables), 13 (Acción por el Clima) y 15 (Vida de Ecosistemas Terrestres).

 - Reducir el impacto medioambiental al disminuir la huella de carbono asociada con la fabricación y el desecho de ordenadores.

 - Controlar el gasto en equipamiento informático, promoviendo el consumo responsable y eficiente de recursos.


### Impacto Ambiental

La fabricación de un ordenador consume una cantidad significativa de recursos y energía. Para fabricar un solo ordenador, se necesita aproximadamente el 70% del consumo eléctrico de su vida útil, lo que equivale a unas 250 toneladas de dióxido de carbono. Además, se requieren unos 240 kg de combustibles fósiles, 22 kg de productos químicos y 1.500 litros de agua. La mayoría de estas materias primas provienen de países en desarrollo, lo que a menudo conduce a la explotación infantil y el trabajo precario.

### Nuestro Compromiso

Como respuesta a estos desafíos ambientales y sociales, hemos tomado medidas concretas:

- **Ampliamos los Tiempos de Renovación:** Si es posible, extendemos los ciclos de vida de nuestros equipos para reducir la generación de residuos electrónicos.

- **Sistematizamos y Regularizamos:** Implementamos un sistema eficiente para gestionar y dar seguimiento a nuestros ordenadores, incluso destinando equipos obsoletos al proyecto de "hardware liberado".

- **Programa de Formación:** Ofrecemos un programa de formación en el uso de equipos personales con Linux, fomentando la adopción de software libre y prácticas sostenibles.

### ¿Cómo Puedes Participar?

Si compartes nuestra visión de un futuro más sostenible y tecnológicamente responsable, te invitamos a unirte a nuestro equipo como **voluntario, colaborador o simplemente a ponerte en contacto con nosotros** para obtener más información. Tu apoyo es fundamental para continuar haciendo una diferencia positiva.

![Equipo](imagen_equipo.jpg)

### Más Información

- Consulta nuestra [página de Web App](/webapp/) para acceder a la plataforma que te permite explorar y adquirir equipos reacondicionados.
- Si deseas formar parte de nuestro proyecto, visita nuestra [página de Únete/Contacto](#unete-contacto) para obtener más detalles sobre cómo involucrarte.
- Descubre recursos útiles relacionados con la gestión sostenible de equipos informáticos en nuestra [página de Recursos](#recursos).

