+++
title = 'Únete al proyecto'
draft = false
id = 'join'
+++

Nuestro proyecto se basa en el apoyo y el compromiso de voluntarios como tú. Si deseas contribuir a la sostenibilidad, la tecnología responsable y la educación, ¡te invitamos a ser parte de nuestro equipo!

### ¿Quiénes Estamos Buscando?


#### Técnicos del SICUZ
Si eres un técnico del Servicio de Informática y Comunicaciones de la Universidad de Zaragoza (SICUZ) y deseas contribuir con tus conocimientos y habilidades, estamos buscando colaboradores de cualquier nivel que estén interesados en trabajar en tareas de apoyo y formación. Tu experiencia es valiosa para el éxito del proyecto.

#### Estudiantes Voluntarios
¡Los estudiantes son una parte fundamental de nuestro equipo! Si  quieres aprender mientras contribuyes a una causa significativa, este proyecto es una oportunidad excelente. Tu entusiasmo y energía son bienvenidos.

#### Estudiantes en Prácticas
Si eres estudiante de Ciclos Formativos o Grados Universitarios y buscas una experiencia práctica en un entorno de trabajo real, te ofrecemos la oportunidad de adquirir habilidades valiosas y trabajar en un proyecto que marca la diferencia.

### Cómo Puedes Contribuir

- **Participa en la revisión y reacondicionamiento de equipos informáticos.**
- **Colabora en la promoción del Software Libre y la sostenibilidad.**
- **Ayuda en la gestión de inventario y la exposición de equipos para su cesión o donación.**
- **Contribuye a la formación de usuarios sobre el uso eficiente de los equipos.**

### Cómo Unirse

Puedes contactarnos en [contáctanos aquí](#).


